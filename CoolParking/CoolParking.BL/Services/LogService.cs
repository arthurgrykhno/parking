﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

namespace CoolParking.BL.Services
{
    using CoolParking.BL.Interfaces;
    using System;
    using System.IO;

    public class LogService : ILogService, IDisposable
    {

        public string LogPath { get; }
        public LogService(string logPath)
        {
            this.LogPath = logPath;
            if (File.Exists(logPath))
                File.Delete(logPath);

            var my = File.Create(logPath);
            my.Close();
        }

        public void Write(string logInfo)
        {
            if (File.Exists(LogPath))
            {
                using (StreamWriter writer = new StreamWriter(LogPath, true))
                {
                    writer.WriteLine(logInfo);
                }
            }

        }

        public string Read()
        {
            if (File.Exists(LogPath))
            {
                using (StreamReader reader = new StreamReader(LogPath))
                {
                    return reader.ReadToEnd();
                }
            }
            throw new InvalidOperationException();
        }

        public void Dispose()
        {
            File.Delete(LogPath);
        }

    }
}