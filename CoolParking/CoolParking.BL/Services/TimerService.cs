﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

namespace CoolParking.BL.Services
{
    using CoolParking.BL.Interfaces;
    using System.ComponentModel;
    using System.IO;
    using System.Timers;

    public class TimerService : ITimerService
    {
        public TimerService()
        {

        }

        Timer timer = new Timer();

        public event ElapsedEventHandler Elapsed
        {
            add { timer.Elapsed += value; }
            remove { timer.Elapsed -= value; }
        }

        public double Interval
        {
            get
            {
                return this.timer.Interval;
            }
            set
            {
                this.timer.Interval = value;
            }
        }


        public void Start()
        {
            timer.Start();
        }
        public void Stop()
        {
            timer.Stop();
        }
        public void Dispose()
        {
            timer.Dispose();
        }

    }
}