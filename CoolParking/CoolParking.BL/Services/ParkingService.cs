﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

namespace CoolParking.BL.Services
{
    using CoolParking.BL.Interfaces;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System;
    using System.Timers;
    using CoolParking.BL.Models;
    using System.Runtime.ConstrainedExecution;

    public class ParkingService : IParkingService
    {
        private Parking parking = Parking.GetParking();

        private ILogService log;
        private ITimerService widthdrawTimer;
        private ITimerService logTimer;

        public ParkingService(ITimerService widthdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this.log = logService;
            this.widthdrawTimer = widthdrawTimer;
            this.widthdrawTimer.Interval = 5000;
            this.widthdrawTimer.Elapsed += Widthdraw;
            this.widthdrawTimer.Start();
            this.logTimer = logTimer;
            this.logTimer.Interval = 60000;
            this.logTimer.Elapsed += CleanTransactions;
            this.logTimer.Start();

        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.Capacity;
        }

        public int GetFreePlaces()
        {
            return Settings.Capacity - parking.OccupiedPlaces;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
        }

        public void AddVehicle(Vehicle vehicle)
        {

            if (parking.OccupiedPlaces < 10)
            {
                
                if (parking.Vehicles.Any(item => item.Id == vehicle.Id))
                    throw new ArgumentException();
                parking.Vehicles.Add(vehicle);
                parking.OccupiedPlaces++;
                string logInfo = vehicle.Id.ToString() + "-" + DateTime.Now + "-" + vehicle.Balance + " поставлено на стоянку";
                log.Write(logInfo);
                Console.WriteLine("Транспортное средство поставлено на стоянку!");
            }
            else
                Console.WriteLine("Нет свободных мест!");
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = parking.Vehicles.FirstOrDefault(item => item.Id == vehicleId);
            if (vehicle == null)
                throw new ArgumentException("");
            if (vehicle.Balance >= 0)
            {
                parking.Vehicles.Remove(vehicle);
                string logInfo = vehicleId.ToString() + " убрано со стоянки" + DateTime.Now;
                log.Write(logInfo);
                parking.OccupiedPlaces--;
            }
            else
                Console.WriteLine("Вы ушли в долг! Пополните, пожалуйста счет транспортного средства!");

        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = parking.Vehicles.FirstOrDefault(item => item.Id == vehicleId);
            if (vehicle == null || sum <= 0)
                throw new ArgumentException("");
            else
            {
                parking.Vehicles.FirstOrDefault(item => item.Id == vehicleId).Balance += sum;
                TransactionInfo transaction = new TransactionInfo
                {
                    VehicleId = vehicleId,
                    Sum = sum,
                    Time = DateTime.Now
                };
                log.Write("Счет № " + transaction.VehicleId + " пополнен на " + sum + " " + transaction.Time);
                TransactionInfo.Transactions[parking.TransactionsCounter] = transaction;
                parking.TransactionsCounter++;

            }

        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return TransactionInfo.Transactions;
        }

        public string ReadFromLog()
        {
            return log.Read();
        }
        public void Dispose()
        {
            this.parking.Dispose();
        }

        public void Widthdraw(object sender, ElapsedEventArgs e)
        {
            foreach (var item in parking.Vehicles)
            {
                var rate = Settings.Rates.FirstOrDefault(o => o.Key == item.VehicleType).Value;
                if (item.Balance > 0)
                {
                    item.Balance -= rate;
                    log.Write("Со счета " + item.Id + " было снято " + rate);
                    parking.Balance += rate;
                    TransactionInfo transaction = new TransactionInfo
                    {
                        VehicleId = item.Id,
                        Sum = rate,
                        Time = DateTime.Now
                    };
                    TransactionInfo.Transactions[parking.TransactionsCounter] = transaction;
                    parking.TransactionsCounter++;

                }

                else
                {
                    item.Balance -= rate * Settings.Penalty;
                    log.Write("Со счета " + item.Id + " было снято " + rate * Settings.Penalty);
                    parking.Balance += rate * Settings.Penalty;
                    TransactionInfo transaction = new TransactionInfo
                    {
                        VehicleId = item.Id,
                        Sum = rate,
                        Time = DateTime.Now
                    };
                    TransactionInfo.Transactions[parking.TransactionsCounter] = transaction;
                    parking.TransactionsCounter++;
                }

            }

        }

        public void CleanTransactions(object sender, ElapsedEventArgs e)
        {
            Array.Clear(TransactionInfo.Transactions, 0, TransactionInfo.Transactions.Length);
            parking.TransactionsCounter = 0;
        }

    }
}