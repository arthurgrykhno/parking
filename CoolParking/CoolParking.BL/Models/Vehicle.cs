﻿// TODO: +implement class Vehicle.
//       +Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       +The format of the identifier is explained in the description of the home task.
//       +Id and VehicleType should not be able for changing.
//       +The Balance should be able to change only in the CoolParking.BL project.
//       +The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       +Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
namespace CoolParking.BL.Models
{
    using System.Text.RegularExpressions;
    using System;
    using System.Linq;

    public class Vehicle
    {
        public string pattern = @"[A-Z][A-Z]-[0-9][0-9][0-9][0-9]-[A-Z][A-Z]";
        public string Id { get; }
        internal VehicleType VehicleType { get; }
        public decimal Balance { get; set; }

        Parking parking = Parking.GetParking();

        public Vehicle(string id, VehicleType vehicle, decimal balance)
        {

            if (parking.Vehicles.Any(item => item.Id == id) || !Regex.IsMatch(id, pattern) || balance < 0)
                throw new ArgumentException();
            else
            {
                this.Id = id;
                this.VehicleType = vehicle;
                this.Balance = balance;
            }
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();
            string result = "";
            string symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            string firstTwo = symbols[random.Next(symbols.Length)].ToString() + symbols[random.Next(symbols.Length)].ToString();
            string secondTwo = symbols[random.Next(symbols.Length)].ToString() + symbols[random.Next(symbols.Length)].ToString();

            result = firstTwo + "-" + random.Next(1000, 9999) + "-" + secondTwo;

            return result;
        }
    }
}