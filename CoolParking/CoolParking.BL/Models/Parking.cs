﻿// TODO: +implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

namespace CoolParking.BL.Models
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class Parking : IDisposable
    {
        private static Parking parking;

        private Parking()
        {
        }

        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles { get; set; }
        public int OccupiedPlaces { get; set; }

        public int TransactionsCounter { get; set; }

        public static Parking GetParking()
        {
            if (parking == null)
            {
                parking = new Parking();
                parking.Balance = Settings.Balance;
                parking.Vehicles = new List<Vehicle>();
                parking.OccupiedPlaces = 0;
                parking.TransactionsCounter = 0;
                return parking;
            }
            return parking;
        }

        public void Dispose()
        {
            Parking.DisposeParking();
        }

        public static void DisposeParking()
        {
            Parking.parking = null;
            Parking.parking = Parking.GetParking();
        }

    }
}