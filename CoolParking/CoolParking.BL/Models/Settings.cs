﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

namespace CoolParking.BL.Models
{
    using System;
    using System.Collections.Generic;
    public static class Settings
    {
        public static int Balance { get; set; } = 0;
        public static int Capacity { get; } = 10;
        public static int PaymentPeriod { get; } = 5;
        public static int LogPeriod { get; } = 60;
        public static decimal Penalty { get; } = 2.5M;

        public static Dictionary<VehicleType, decimal> Rates = new Dictionary<VehicleType, decimal>()
        {
            { VehicleType.PassengerCar, 2},
            { VehicleType.Truck, 5 },
            { VehicleType.Bus, 3.5M },
            { VehicleType.Motorcycle, 1 }

        };
    }
}