﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
namespace CoolParking.BL.Models
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public struct TransactionInfo
    {
        public decimal Sum { get; set; }
        public DateTime Time { get; set; }
        public string VehicleId { get; set; }
        public static TransactionInfo[] Transactions { get; set; } = new TransactionInfo[100];

        public override string ToString()
        {
            return $"Транспортное средство: {VehicleId}  сумма транзакции:  {Sum}  время: {Time}";
        }
    }
}
