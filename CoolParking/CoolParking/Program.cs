﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using FakeItEasy;
using System;
using System.IO;
using System.Linq;

namespace CoolParking
{
    class Program
    {
        static void Main(string[] args)
        {
            //обьект парковки
            Parking parking = Parking.GetParking();
            Parking gg;

            TimerService widthdrawTimer = new TimerService();
            TimerService logTimer = new TimerService();
            string logPath = Path.Combine(Environment.CurrentDirectory, "Transactions.log");
            
            
            LogService log = new LogService(logPath);

            ParkingService parkingService = new ParkingService(widthdrawTimer, logTimer, log);
            
 

            while(true)
            {
                string result = "";
                ShowMenu();
                result = Console.ReadLine();
                ShowResult(result, parkingService);
            }

        }

        private static void ShowMenu()
        {
            Console.WriteLine("Здравствуйте, что Вы хотите сделать?");
            Console.WriteLine("1.Посмотреть баланс парковки");
            Console.WriteLine("2.Посмотреть заработанную сумму за текущий период");
            Console.WriteLine("3.Посмотреть кол-во занятых/свободных мест");
            Console.WriteLine("4.Посмотреть последнии транзакции");
            Console.WriteLine("5.Посмотреть историю транзакций");
            Console.WriteLine("6.Посмотреть все транспортные средства");
            Console.WriteLine("7.Поставить транспортное средство на паркинг");
            Console.WriteLine("8.Забрать транспортное средство с паркинга");
            Console.WriteLine("9.Пополнить баланс транспортного средства");
        }

        private static void ShowResult(string result, ParkingService parkingService)
        {
            Console.WriteLine(new string('-', 50)); 
            switch(result)
            {
                case "1": 
                    Console.WriteLine("Баланс парковки: " + Parking.GetParking().Balance); 
                    break;
                case "2": 
                    decimal sum = 0; 
                    foreach(var item in parkingService.GetLastParkingTransactions())
                    {
                        sum += item.Sum;
                    }
                    Console.WriteLine("За текущий период заработано: " + sum);
                    break;
                case "3":
                    Console.WriteLine("Свободных мест: " + parkingService.GetFreePlaces() + " из 10");
                    break;
                case "4":
                    foreach(var item in parkingService.GetLastParkingTransactions().Where(i => i.VehicleId != null))
                    {
                        Console.WriteLine(item.ToString());
                    }
                    break;
                case "5":
                    Console.WriteLine(parkingService.ReadFromLog());
                    break;
                case "6":
                    Console.WriteLine("Машины на парковке: ");
                    foreach(var item in parkingService.GetVehicles())
                    {
                        Console.WriteLine(item.Id);
                    }
                    break;
                case "7":
                    Console.WriteLine("Тип транспортного средства: ");
                    Console.WriteLine("1.Легковое");
                    Console.WriteLine("2.Грузовое");
                    Console.WriteLine("3.Автобус");
                    Console.WriteLine("4.Мотоцикл");
                    string res = Console.ReadLine();
                    VehicleType vehicleType = new VehicleType();
                    switch (res)
                    {
                        case "1": vehicleType = VehicleType.PassengerCar; break;
                        case "2": vehicleType = VehicleType.Truck; break;
                        case "3": vehicleType = VehicleType.Bus; break;
                        case "4": vehicleType = VehicleType.Motorcycle; break;
                        default: throw new ArgumentException("Нет такого типа траспорта!");
                    }
                    Console.WriteLine("Баланс: ");
                    decimal balance = Convert.ToDecimal(Console.ReadLine());
                    Vehicle vehicle = new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), vehicleType, balance);
                    parkingService.AddVehicle(vehicle);
                    Console.WriteLine("Номер вашего транспортного средства: " + vehicle.Id);
                    break;
                case "8":
                    Console.WriteLine("Введите номер транспортного средства");
                    string removeId = Console.ReadLine();
                    parkingService.RemoveVehicle(removeId);
                    break;
                case "9":
                    Console.WriteLine("Введите номер транспортного средства: ");
                    string topUpId = Console.ReadLine();
                    Console.WriteLine("На сколько пополняем?");
                    decimal topUpSum = Convert.ToDecimal(Console.ReadLine());
                    parkingService.TopUpVehicle(topUpId, topUpSum);
                    break;
            }
            Console.WriteLine(new string('-', 50));
        }
    }


}
